var five = require('johnny-five');
var Raspi = require('raspi-io');
var keypress = require('keypress');
var temporal = require("temporal");

const ports = [
  {id: 'pi', io: new Raspi({includePins: ['GPIO21'], enableSerial: true}) },
  {id: 'arduino', port: '/dev/ttyUSB0'}
];

var boards = new five.Boards(ports);

boards.on('ready', function() {

  const driveMotors = new five.Motors([
    {pins: [3, 5], invertPWM: true, board: this.byId('arduino')},
    {pins: [10, 11], invertPWM: true, board: this.byId('arduino')},
    {pins: [6, 9], invertPWM: true, board: this.byId('arduino')}
  ]);

  driveMotors.stop();

  //var esc = new five.ESC({pin: 2, board: this.byId('arduino')});

  var imu = new five.IMU({controller: 'LSM303C', board: this.byId('arduino')});

  var gps = new five.GPS({pins: {tx: 8, rx: 7}, controller: 'ADAFRUIT_ULTIMATE_GPS', board: this.byId('arduino')});

  console.log('everything initialized well!!! hahaha, come back tomorrow.');

  function heading(gps_init,gps_end) {
    console.log("calcing heading");
    var dLon = (gps_end[1]-gps_init[1]);
    var y = Math.sin(dLon) * Math.cos(gps_end[0]);
    var x = Math.cos(gps_init[0])*Math.sin(gps_end[0]) - Math.sin(gps_init[0])*Math.cos(gps_end[0])*Math.cos(dLon);
    var bearing = toDeg(Math.atan2(y, x));
    return 360 - ((bearing + 360) % 360);
  }

  function toRad(deg) {
    return deg * Math.PI / 180;
  }

  function toDeg(rad) {
    return rad * 180 / Math.PI;
  }

  function toLocation([lat, long]) {
    const error = 0.000001;

    temporal.loop(1000, function() {
      console.log("Every 500ms...");

      const isBelowLatitude = gps.latitude - error > lat;
      const isAboveLatitude = gps.latitude + error < lat;
      const isBelowLongitude = gps.longitude - error > long;
      const isAboveLongitude = gps.longitude + error < long;

      if (this.called < 30 && (isBelowLatitude || isAboveLatitude || isBelowLongitude || isAboveLongitude)) {
        toOrientation(heading([gps.latitude, gps.longitude], [lat, long]));
        drive(1, 0, 0);
      } else {
        drive(0, 0, 0);
        drive(0, 0, 0);
        drive(0, 0, 0);
        this.stop();

        return true;
      }
    });
  }

  function getLocation() {
    return [gps.latitude, gps.longitude];
  }

  function toOrientation(angle) {
    const offset = -161.3572090935;
    const error = 10;

    temporal.loop(500, function() {
      console.log("Every 500ms...");

      const isBelowTolerance = angle < (Number(imu.magnetometer.heading)+offset+720)%360 - error;
      const isAboveTolerance = angle > (Number(imu.magnetometer.heading)+offset+720)%360 + error;

      if (this.called < 20 && (isBelowTolerance || isAboveTolerance)) {
        console.log(imu.magnetometer.raw);
        console.log(imu.magnetometer.heading);
        if ((imu.magnetometer.heading+offset) % 360 - angle > 0) {
          console.log("driving one way");
          drive(0, 0, 0.45);
        } else {
          console.log("driving the other way");
          drive(0, 0, -0.45);
        }
      } else {
        drive(0, 0, 0);
        drive(0, 0, 0);
        drive(0, 0, 0);
        this.stop();

        return true;
      }
    });

    return true;
  }

function getOrientation() {
    const offset = -161.3572090935
    return (imu.magnetometer.heading%360+offset+360)%360
  }

  function cutter(bool) {
    var speed_sub = bool? 20:100;
    esc.speed(speed_sub);
    return (1);
  }

  function drive(x, y, rot) {
    const rotationConstant = 1;
    const driveConstant = 1;
    var W1 = ((-1/2*x - y*(3**0.5)/2)*driveConstant + rot*rotationConstant)*255;
    var W2 = ((-1/2*x + y*(3**0.5)/2)*driveConstant + rot*rotationConstant)*255;
    var W3 = (x*driveConstant*1.75 + rot*rotationConstant)*255;

    W1 = W1 >  255 ?  255 : W1;
    W1 = W1 < -255 ? -255 : W1;

    W2 = W2 >  255 ?  255 : W2;
    W2 = W2 < -255 ? -255 : W2;

    W3 = W3 >  255 ?  255 : W3;
    W3 = W3 < -255 ? -255 : W3;

    if (W1==0) {
      driveMotors[0].stop();
    } else if (W1>0) {
      driveMotors[0].forward(W1);
    } else if (W1<0) {
      driveMotors[0].reverse(Math.abs(W1));
    }
    if (W2==0) {
      driveMotors[1].stop();
    } else if (W2>0) {
      driveMotors[1].forward(W2);
    } else if (W2<0) {
      driveMotors[1].reverse(Math.abs(W2));
    }
    if (W3==0){
      driveMotors[2].stop();
    } else if (W3>0) {
      driveMotors[2].forward(W3);
    } else if (W3<0) {
      driveMotors[2].reverse(Math.abs(W3));
    }
  }

  this.repl.inject({
    // Allow limited on/off control access to the
    // Led instance from the REPL.
    getLocation: function() {
      return getLocation()
    },
    toLocation: function(place) {
      toLocation(place)
    },
    getOrientation: function() {
      return getOrientation()
    },
    toOrientation: function(orientation) {
      toOrientation(orientation)
    },
    drive: function(x, y, rot) {
      drive(x, y, rot);
    },
    driveInASquare: function() {
      driveInASquare();
    },
    turnRight: function(number) {
      turnRight(number);
    },
    driveStraight: function(time) {
      driveStraight(time);
    }
  });

  function driveStraight(time) {
    let currentHeading = imu.magnetometer.heading;
    let error = 10;
    let turnConstant = 1;
    let duration = 250;
    temporal.loop(duration, function() {
      console.log("Every "+duration+"ms...");
      if (this.called >= time) {
        drive(0, 0, 0);
        drive(0, 0, 0);
        drive(0, 0, 0);
        this.stop();
      } else {
        let mult;
        if (this.called%2 === 1) {
          mult = -1;
        } else {
          mult = 1;
        }
        drive(0, -1, turnConstant*(imu.magnetometer.heading-currentHeading));
      }
    });
  }

  function turnRight() {
    const duration = 250;
    temporal.loop(duration, function() {
      console.log("Every "+duration+"ms...");
      if (this.called >= 4) {
        drive(0, 0, 0);
        drive(0, 0, 0);
        drive(0, 0, 0);
        this.stop();
      } else {
        drive(0, 0, -1);
      }
    });
  }

  function driveInASquare() {
    driveStraight(16);
    console.log(getLocation())
    turnRight();
    driveStraight(16);
    console.log(getLocation())
    turnRight();
    driveStraight(16);
    console.log(getLocation())
    turnRight();
    driveStraight(16);
    console.log(getLocation())
    turnRight();
  }


  module.exports.toLocation = toLocation;
  module.exports.getLocation = getLocation;
  module.exports.toOrientation = toOrientation;
  module.exports.cutter = cutter;
});
