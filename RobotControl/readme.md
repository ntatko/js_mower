Installation:

  1.) Johnny Five:

        npm update -g npm:latest
        npm install -save Johnny-Five

  2.) RaspIO:

        npm install -save raspi-io

  3.) Raspi-live (Docs: https://github.com/jaredpetersen/raspi-live):
    a. FFMPEG -

        sudo apt-get install libomxil-bellagio-dev
        wget -O ffmpeg.tar.bz2 https://ffmpeg.org/releases/ffmpeg-snapshot-git.tar.bz2
        tar xvjf ffmpeg.tar.bz2
        cd ffmpeg
        sudo ./configure --arch=armel --target-os=linux --enable-gpl --enable-omx --enable-omx-rpi --enable-nonfree
        sudo make -j4
        sudo make install

    b. Raspi-Live -

        npm install -save raspi-live

  4.) Keypress:

        npm install -save keypress

Use:

  It's not hard to use the functions here. There are three functions:

  getLocation()
  toLocation([lat, long])
  cutter(false/true)

  That's all there really is to it. Import the robotHardwareFunctions.js file into
  your project, and use them.
