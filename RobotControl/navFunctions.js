var kalman = require("kalmanjs").default;
var pid_controller = require("node-pid-controller");

module.exports{
  // this is a simple function to determine if the mower is within the bounds of the prespecified lawn.
  // Some stackoverflow user with Nasa in his name added it, so I assume it's good.
  function within(perim_verticies, current_pos) {
    const x = current_pos[0];
    const y = current_pos[1];

    var inside = false;
    for (let i = 0, j = perim_verticies.length - 1; i < perim_verticies.length; j = i++) {
      let xi = perim_verticies[i][0];
      let yi = perim_verticies[i][1];
      let xj = perim_verticies[j][0];
      let yj = perim_verticies[j][1];

      let intersect = ((yi > y) != (yj > y)) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
      if (intersect) {
        inside = !inside;
      }
    }
    return inside;
  }

  // I probably won't use this function, as what I plan on doing for obsticles is
  // going right up to them and hovering about an inch away from them, as to provide
  // a closer cut.
  function path_is_blocked(distance_arr) {
    var blocked = false;
    const tooClose = 10;
    for (let i = 0; i < sensor_arr.length; i++) {
      if (distance_arr <= tooClose) {
        blocked = !blocked;
      }
    }
    return blocked;
  }

  // These functions are designed to get the compass heading between two points.
  // Now, this is not a great idea, because there is so much variability and inaccuracy
  // in the regular data gotten from a compass. The hope is that the below kalman
  // filter an addition to some sensor fusion, can assist this process to be
  // more accurate.
  function heading(gps_init,gps_end) {
    var dLon = (gps_end[1]-gps_init[1]);
    var y = Math.sin(dLon) * Math.cos(gps_end[0]);
    var x = Math.cos(gps_init[0])*Math.sin(gps_end[0]) - Math.sin(gps_init[0])*Math.cos(gps_end[0])*Math.cos(dLon);
    var bearing = toDeg(Math.atan2(y, x));
    return 360 - ((bearing + 360) % 360);
  }

  function toRad(deg) {
    return deg * Math.PI / 180;
  }

  function toDeg(rad) {
    return rad * 180 / Math.PI;
  }

  // a basic 1DX1D kalman filter for GPS points. It's not great, as a real kalman
  // filter would take into account the total distance travelled, not just the
  // distance in either direction, but I can fix that later, this implimentation
  // is far simpler.
  var kalman_lat = new KalmanFilter({R: 0.01, Q: 3, A: 1});
  var kalman_long = new KalmanFilter({R: 0.01, Q: 3, A: 1});
  function smoothGPSData(point) {
    return [kalman_lat.filter(point[0]), kalman_long.filter(point[1])];
  }

  // This is going to be a PID alrogithm (Proportional, Integral, Derivative)
  // to smooth the incoming sensor data. I realize that it would probably be better
  // to use a Kalman filter, but as I understand PID better and the general result
  // will be similar, here we go.
  // note: the constants will need to be adjusted over time.
  var acc_x = new Controller(0.25, 0.01, 0.01, 1);
  acc_x.setTarget(0);
  var acc_y = new Controller(0.25, 0.01, 0.01, 1);
  acc_y.setTarget(0);
  var acc_z = new Controller(0.25, 0.01, 0.01, 1);
  acc_z.setTarget(0);

  var comp_x = new Controller(0.25, 0.01, 0.01, 1);
  comp_x.setTarget(0);
  var comp_y = new Controller(0.25, 0.01, 0.01, 1);
  comp_x.setTarget(0);
  var comp_z = new Controller(0.25, 0.01, 0.01, 1);
  comp_x.setTarget(0);
  function updateSensorData(data, which) {
    if (which === "accelerometer") {
      return [acc_x.update(data[0]), acc_y.update(data[1]), acc_z.update(data[2])];
    }
    if (which === "compass") {
      return [comp_x.update(data[0]), comp_y.update(data[1]), comp_z.update(data[2])];
    }
  }

  // This function is designed to select the next waypoint in the polygon,
  // so that a destination can be found and used.
  function select_waypoint(polygon, position, heading) {

  }
}
