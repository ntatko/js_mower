var five = require('johnny-five');
var Raspi = require('raspi-io');

const ports = [
  {id: 'pi', io: new Raspi({includePins: ['GPIO21'], enableSerial: true}) },
  {id: 'arduino', port: '/dev/ttyUSB0'}
];

let ranges;

var boards = new five.Boards(ports);

boards.on('ready', function() {
  var imu = new five.IMU({controller: 'LSM303C', board: this.byId('arduino')});


  function calibrateCompass() {
    ranges = {x: {low: 360, high: 0}, y: {low: 360, high: 0}, z: {low: 360, high: 0}};
    imu.on("change", function() {
      if(this.magnetometer) {
        //x max
        ranges.x.high = ranges.x.high < this.magnetometer.raw.x ? this.magnetometer.raw.x : ranges.x.high;
        //x min
        ranges.x.low = ranges.x.low > this.magnetometer.raw.x ? this.magnetometer.raw.x : ranges.x.low;
        //y max
        ranges.y.high = ranges.y.high < this.magnetometer.raw.y ? this.magnetometer.raw.y : ranges.y.high;
        //y min
        ranges.y.low = ranges.y.low > this.magnetometer.raw.y ? this.magnetometer.raw.y : ranges.y.low;
        //z max
        ranges.z.high = ranges.z.high < this.magnetometer.raw.z ?  this.magnetometer.raw.z : ranges.z.high;
        //z min
        ranges.z.low = ranges.z.low > this.magnetometer.raw.z ? this.magnetometer.raw.z : ranges.z.low;
        //console.log(Math.atan(transformY(this.magnetometer.raw.y), transformX(this.magnetometer.raw.x)));
        //console.log(Math.acos(transformX(this.magnetometer.raw.x)) + ", " + Math.asin(transformY(this.magnetometer.raw.y)));
        console.log(this.magnetometer.heading);
      }
    })
  }

  function transformX(initial) {
    return 2*(initial-ranges.x.low)/(ranges.x.high-ranges.x.low) - 1
  }

  function transformY(initial) {
    return 2*(initial-ranges.y.low)/(ranges.y.high-ranges.y.low) - 1
  }



  calibrateCompass();

});
