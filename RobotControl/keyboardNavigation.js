var five = require("johnny-five");
var keypress = require("keypress");
var Raspi = require("raspi-io");
var board = new five.Board({io: new Raspi(
  { enableSoftPwm: true, includePins: ['GPIO4', 'GPIO17', 'GPIO27', 'GPIO22', 'GPIO9', 'GPIO11', 'GPIO18'] }
)});

board.on("ready", function() {
  var motors = new five.Motors([
    {pins: {pwm: "GPIO4", dir: "GPIO17"}, invertPWM: true, enableSoftPwm: true},
    {pins: {pwm: "GPIO27", dir: "GPIO22"}, invertPWM: true, enableSoftPwm: true},
    {pins: {pwm: "GPIO9", dir: "GPIO11"}, invertPWM: true, enableSoftPwm: true},
  ]);

  var esc = new five.ESC({pin: "GPIO18", enableSoftPwm: true});

  function stop() {
    motors[0].stop();
    motors[1].stop();
    motors[2].stop();
  }

  stop();
  process.stdin.on("keypress", function(ch, key) {
    console.log(key.name);
    if(key.name === "up") {
      stop();
      motors[1].forward(225);
      motors[2].reverse(225);
    } else if (key.name === "down") {
      stop();
      motors[1].reverse(225);
      motors[2].forward(225);
    } else if (key.name === 'right') {
      stop();
      motors[0].forward(225);
      motors[1].forward(225);
      motors[2].forward(225);
    } else if (key.name === 'left') {
      stop()
      motors[0].reverse(225);
      motors[1].reverse(225);
      motors[2].reverse(225);
    } else {
      stop();
    }
  });
});
