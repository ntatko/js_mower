import React from 'react';
import ReactDOM from 'react-dom';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";
//import { getLocation, toLocation, cutter } from "./../RobotControl/robotHardwareFunctions.js"


const title = 'My Minimal React Webpack Babel Setup';
const gpsCords = [38.641378,-90.315963];

const MyMapComponent = withScriptjs(withGoogleMap((props) =>
  <GoogleMap
    defaultZoom={20}
    center={{ lat: props.lati, lng: props.longi }}
  >
    {props.isMarkerShown && <Marker position={{ lat: props.lati, lng: props.longi }} />}
  </GoogleMap>
))



class Map extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <MyMapComponent
        isMarkerShown
        lati={this.props.latitude}
        longi={this.props.longitude}
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAejbzDNZSiTeg4VfWHXsp2RqmBB032MvI&v=3.exp&libraries=geometry,drawing,places"
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `400px` }} />}
        mapElement={<div style={{ height: `100%` }} />}
      />
    )
  }
}

class StartStop extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <div>
        <button type="button">Start</button>
        <button type="button">Stop</button>
      </div>
    )
  }
}

class Latitude extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <div>
        Latitude: <div contentEditable="True" onChange={(value)=>{this.props.change(value)}}>{this.props.latitude}</div>
      </div>
    )
  }
}

class AppJS extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      curr_latitude: 0,
      curr_longitude: 0,
      dest_latitude: 38.641378,
      dest_longitude: -90.315963
    }

    this.changeDestLatitude = this.changeDestLatitude.bind(this)
    this.changeDestLongitude = this.changeDestLongitude.bind(this)
  }

  changeDestLatitude(e) {
    this.setState({dest_latitude: Number(e.target.value)})
  }

  changeDestLongitude(e) {
    this.setState({dest_longitude: Number(e.target.value)})
  }

  onChange(field, value) {
    // parent class change handler is always called with field name and value
    this.setState({[field]: value});
  }

  render() {
    return (
      <div>
        Latitude: <input value={this.state.dest_latitude} onChange={this.changeDestLatitude} />
        <div>{this.state.dest_latitude}</div>
        <Map latitude={this.state.dest_latitude} longitude={this.state.dest_longitude} />
        <StartStop />
      </div>
    )
  }
}

ReactDOM.render(
  <AppJS />,
  document.getElementById('app')
);

module.hot.accept();
